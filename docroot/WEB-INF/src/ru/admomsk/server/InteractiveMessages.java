package ru.admomsk.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletRequest;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.FileUtils;

import ru.admomsk.server.beans.EditBean;
import ru.admomsk.server.beans.ViewBean;
import ru.admomsk.utils.MyUtils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;

/**
 * Portlet implementation class InteractiveMessages
 */
public class InteractiveMessages extends GenericPortlet {

	protected static EditBean editBean;
	protected String editJSP;
    protected String viewJSP;
    private static Log _log = LogFactoryUtil.getLog(InteractiveMessages.class);
    PortletURL viewURL = null;
    
    @Override
    public void init() {
    	editBean = new EditBean();
        editJSP = getInitParameter("edit-jsp");
        viewJSP = getInitParameter("view-jsp");
    }
    
    @Override
    public void processAction(
            ActionRequest actionRequest, ActionResponse actionResponse)
        throws IOException, PortletException {
    	
    	
    	PortletSession portletSession = actionRequest.getPortletSession();
    	
    	if ((ViewBean)portletSession.getAttribute("viewBean") == null){
    		portletSession.setAttribute("viewBean", new ViewBean());
    	}
    	
    	
    	if (editBean == null) {
    		editBean = new EditBean();
    	}
    	
    	if (actionRequest.getPortletMode().toString().equalsIgnoreCase("edit")) {
    		
    		editBean.setHost(actionRequest.getParameter("email_smpt_inter_mess"));
    		editBean.validateHost();
    		
    		editBean.setPort(actionRequest.getParameter("email_port_inter_mess"));
    		editBean.validatePort();
    		
    		editBean.setTo(actionRequest.getParameter("email_sed_inter_mess"));
    		editBean.validateTo();
    		
    		editBean.setFrom(actionRequest.getParameter("email_from_inter_mess"));
    		editBean.validateFrom();
    		
    		editBean.setPassFrom(actionRequest.getParameter("pass_email_from_inter_mess"));
    		
    		editBean.setMaxSizeAttach(actionRequest.getParameter("max_size_attach_inter_mess"));
    		editBean.validateMaxSizeAttach();
    		
    		editBean.setFormatAttach(actionRequest.getParameter("format_attach_inter_mess"));
    		
    		editBean.setAdress(actionRequest.getParameter("is_adress_inter_mess"));
    		
    		editBean.setUrlVerify(actionRequest.getParameter("url_verify_inter_mess"));
    		
    		editBean.setSiteKey(actionRequest.getParameter("site_key_inter_mess"));
    		
    		editBean.setSecretKey(actionRequest.getParameter("secret_key_inter_mess"));
    		
    		editBean.setIp(actionRequest.getParameter("is_ip_inter_mess"));
    		
    	}
    	
    	if (actionRequest.getPortletMode().toString().equalsIgnoreCase("view")) {
    		
    		((ViewBean)portletSession.getAttribute("viewBean")).resetValue();
    		((ViewBean)portletSession.getAttribute("viewBean")).resetValid();
    		
    		((ViewBean)portletSession.getAttribute("viewBean")).setViewForm(true);
    		
    		//получить свойства портлета
        	editBean.setPortletPreferences(actionRequest.getPreferences());
    		
    		((ViewBean)portletSession.getAttribute("viewBean")).setEditBean(editBean);
    		
    		PortalUtil.getPortalProperties().setProperty("com.liferay.portal.upload.UploadServletRequestImpl.max.size", String.valueOf(Integer.MAX_VALUE));
    		PortalUtil.getPortalProperties().setProperty("com.liferay.portal.upload.UploadServletRequestImpl.max.size", "104857600"); 
    		
			FileItemFactory factory = new DiskFileItemFactory();
			PortletFileUpload upload = new PortletFileUpload(factory);
			upload.setHeaderEncoding(actionRequest.getCharacterEncoding());
			
			File uploadFile = null;
			
			try {
				
				if (actionRequest.getContentType().indexOf("multipart/form-data") > -1) {
					
					FileItemIterator fii = upload.getItemIterator(actionRequest);
					FileItemStream fis = null;
					FileItemStream fisUploadFile = null;
				
					//просмариваем все присланные поля
					while (fii.hasNext()) {
						fis = fii.next();
						
						//если поле - присланный файл
						if (!fis.isFormField()) {
							fisUploadFile = fis;
							
							//Если пришел какой-то файл
		                	if (!fisUploadFile.getName().equals("")){
		                		uploadFile = File.createTempFile("uploadFile", MyUtils.getExt(fisUploadFile.getName()));
		                		//размер части сколько читать в файл в килобайтах
		                		int partData = 32*1024;
		                		FileOutputStream fos = new FileOutputStream(uploadFile);
		                		BufferedOutputStream bos = new BufferedOutputStream(fos, partData);
		                		BufferedInputStream bis = new BufferedInputStream(fisUploadFile.openStream(), partData);
		                		
		                		int c;
		                		byte data[] = new byte[partData];
		                		
		                		//while (((c = bis.read(data)) != -1) && (Double.valueOf(String.valueOf(uploadFile.length())) < ((EditBean)portletSession.getAttribute("editBean")).getMaxSizeAttachInDouble()) ) {
		                		while (((c = bis.read(data)) != -1) && (Double.valueOf(String.valueOf(uploadFile.length())) < editBean.getMaxSizeAttachInDouble()) ) {
		                			bos.write(data, 0, c);
		                			bos.flush();
		                		}
		                		
		                		//всё закрыть
		                		bos.close();
		                		bis.close();
		                		fos.close();
		                    	
		                	}
						
		                } else {//обычные поля
		                	
		                	BufferedReader br = new BufferedReader(new InputStreamReader(fis.openStream(), "UTF-8"));
		                	
		                	String line = null;
		                	StringBuffer lines = new StringBuffer();
		                	int countLines = 0;
		                	while ((line = br.readLine()) != null) {
		                		countLines++;
		                		if (countLines >= 2){
		                			lines.append("\n");
		                		}
		                		lines.append(line);
		                	}
		                	br.close();
		                	
		                	//в соотвествии с названием поля
		                	if (fis.getFieldName().equals("dep_code_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setDepCode(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("surname_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setSurname(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("firstname_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setFirstname(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("middlename_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setMiddlename(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("index_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setIndex(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("city_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setCity(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("street_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setStreet(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("home_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setHome(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("housing_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setHousing(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("apartment_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setApartment(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("organization_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setOrganization(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("number_phone_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setNumberPhone(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("email_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setEmail(lines.toString());
		                	}
		                	if (fis.getFieldName().equals("text_appeal_inter_mess")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setTextAppeal(lines.toString());
		                	}
		                	
		                	if (fis.getFieldName().equals("g-recaptcha-response")) {
		                		((ViewBean)portletSession.getAttribute("viewBean")).setReCaptcha(lines.toString());
		                	}
		                	
		                	
		                }
						
					}
					
				} else {
					
					//в соотвествии с названием поля
					((ViewBean)portletSession.getAttribute("viewBean")).setSurname(actionRequest.getParameter("surname_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setFirstname(actionRequest.getParameter("firstname_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setMiddlename(actionRequest.getParameter("middlename_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setIndex(actionRequest.getParameter("index_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setCity(actionRequest.getParameter("city_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setStreet(actionRequest.getParameter("street_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setHome(actionRequest.getParameter("home_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setHousing(actionRequest.getParameter("housing_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setApartment(actionRequest.getParameter("apartment_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setOrganization(actionRequest.getParameter("organization_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setNumberPhone(actionRequest.getParameter("number_phone_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setEmail(actionRequest.getParameter("email_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setTextAppeal(actionRequest.getParameter("text_appeal_inter_mess"));
					((ViewBean)portletSession.getAttribute("viewBean")).setReCaptcha(actionRequest.getParameter("g-recaptcha-response"));
					
				}
				
			} catch (FileUploadException e1) {
				// TODO Auto-generated catch block
				System.out.println("InteractiveMessages: FileUploadException");
				e1.printStackTrace();
			}  finally{
				
		    	HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(actionRequest); 
		    	String ipRemote = httpServletRequest.getRemoteAddr();
				
		    	//Провалидировать поля
				((ViewBean)portletSession.getAttribute("viewBean")).validateSurname();
				((ViewBean)portletSession.getAttribute("viewBean")).validateFirstname();
				((ViewBean)portletSession.getAttribute("viewBean")).validateEmail();
				((ViewBean)portletSession.getAttribute("viewBean")).validateTextAppeal();
				((ViewBean)portletSession.getAttribute("viewBean")).validateReCaptcha(ipRemote);
				
				//если файл пришел от клиента и существует
				if (uploadFile != null) {
					if (uploadFile.exists()) {
						//установить файл
		            	((ViewBean)portletSession.getAttribute("viewBean")).setAttach(uploadFile, uploadFile.getName());
		            	//провалидировать файл
		            	((ViewBean)portletSession.getAttribute("viewBean")).validateAttach();
		            	
		            	//если есть поля которые провалидированны неудачно, то удалить всё что связанно с attach
						if (!( 
						((ViewBean)portletSession.getAttribute("viewBean")).getValidSurname().equals("ok") && 
						((ViewBean)portletSession.getAttribute("viewBean")).getValidFirstname().equals("ok") &&
						((ViewBean)portletSession.getAttribute("viewBean")).getValidEmail().equals("ok") &&
						((ViewBean)portletSession.getAttribute("viewBean")).getValidTextAppeal().equals("ok") &&
						((ViewBean)portletSession.getAttribute("viewBean")).getValidReCaptcha().equals("ok")
						)) {
							((ViewBean)portletSession.getAttribute("viewBean")).removeAttach();
						}
						
						//если upload всё еще существует, то удалить его
						if (uploadFile.exists()) {
							FileUtils.forceDelete(uploadFile);
						}
					
					}
				}
				
				//Если провалидированно успешно
				if (((ViewBean)portletSession.getAttribute("viewBean")).isValidAll()) {
					((ViewBean)portletSession.getAttribute("viewBean")).setViewForm(false);
					
					//если не создан renderURL(нужен для генерации ссылки активации в письме), то создать его
					if (((ViewBean)portletSession.getAttribute("viewBean")).getRenderURL() == null) {
						
			    		String portletName = (String)actionRequest.getAttribute(WebKeys.PORTLET_ID);
			        	ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			        	PortletURL renderURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(actionRequest),portletName,
			        	themeDisplay.getLayout().getPlid(), PortletRequest.RENDER_PHASE);
			        	renderURL.setParameter("i_m_id", "null");
			    		((ViewBean)portletSession.getAttribute("viewBean")).setRenderURL(renderURL.toString());
					} else {
					
						if (((ViewBean)portletSession.getAttribute("viewBean")).getRenderURL().equals("")) {
						
				    		String portletName = (String)actionRequest.getAttribute(WebKeys.PORTLET_ID);
				        	ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
				        	PortletURL renderURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(actionRequest),portletName,
				        	themeDisplay.getLayout().getPlid(), PortletRequest.RENDER_PHASE);
				        	renderURL.setParameter("i_m_id", "null");
				    		((ViewBean)portletSession.getAttribute("viewBean")).setRenderURL(renderURL.toString()); 
						}
					}
							
					//послать сообщение
					if (((ViewBean)portletSession.getAttribute("viewBean")).sendMessageToUser()) {
						((ViewBean)portletSession.getAttribute("viewBean")).setSentStatus(ViewBean.SENT_USER_SUCC);
					} else {
						((ViewBean)portletSession.getAttribute("viewBean")).setSentStatus(ViewBean.SENT_USER_FAIL);
					}
					
	    		}
				
				((ViewBean)portletSession.getAttribute("viewBean")).setProcessing(true);
			}
		
    	}
		
    }
    
    @Override
    public void render(RenderRequest request, RenderResponse response)
    		throws PortletException, IOException {
    	
    	// TODO Auto-generated method stub
    	super.render(request, response);
    }
    
    @Override
    protected void doEdit(RenderRequest renderRequest, RenderResponse renderResponse)
    		throws PortletException, IOException {
    	
    	if (editBean == null){
    		editBean = new EditBean();
    	}
    	
    	editBean.setPortletPreferences(renderRequest.getPreferences());
    	renderRequest.setAttribute("editBean", editBean);
		
    	include(editJSP, renderRequest, renderResponse);
    	
    }
    
    @Override
    public void doView(
            RenderRequest renderRequest, RenderResponse renderResponse)
        throws IOException, PortletException {
    	
    	if (viewURL == null) {
    		viewURL = renderResponse.createRenderURL();
    		viewURL.setPortletMode(PortletMode.VIEW);
    	}
    	PortletSession portletSession = renderRequest.getPortletSession();
    	
    	if ((ViewBean)portletSession.getAttribute("viewBean") == null){
    		portletSession.setAttribute("viewBean", new ViewBean());
    	}
    	
    	if (editBean == null) {
    		editBean = new EditBean();
    	}
    	
    	//получить свойства портлета
    	editBean.setPortletPreferences(renderRequest.getPreferences());
    	((ViewBean)portletSession.getAttribute("viewBean")).setEditBean(editBean);
    	
		//установить renderURL, для ссылки активации
    	if (((ViewBean)portletSession.getAttribute("viewBean")).getRenderURL().equals("")) {
    		
    		String portletName = (String)renderRequest.getAttribute(WebKeys.PORTLET_ID);
        	ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	PortletURL renderURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(renderRequest),portletName,
        	themeDisplay.getLayout().getPlid(), PortletRequest.RENDER_PHASE);
        	renderURL.setParameter("i_m_id", "null");
    		((ViewBean)portletSession.getAttribute("viewBean")).setRenderURL(renderURL.toString());
    	}
    	
    	//если пришел параметр активации ссылки пользователем
    	if (renderRequest.getParameter("i_m_id") != null ) {
    		if (!renderRequest.getParameter("i_m_id").equals("") ) {
    			
    			((ViewBean)portletSession.getAttribute("viewBean")).setViewForm(false);
    			//послать сообщение в СЕД
				if ( ((ViewBean)portletSession.getAttribute("viewBean")).sendMessageToSed(renderRequest.getParameter("i_m_id")) ) {
					((ViewBean)portletSession.getAttribute("viewBean")).setSentStatus(ViewBean.SENT_SED_SUCC);
				} else {
					((ViewBean)portletSession.getAttribute("viewBean")).setSentStatus(ViewBean.SENT_SED_FAIL);
				}
    		}
    	}
    	
    	/*Если нажали кнопку назад при успешной отправке сообщения, то очистить все поля
    	условие под заглушкой, необходимо, если будет кнопка в успешно отправленном сообщении*/
    	if (((ViewBean)portletSession.getAttribute("viewBean")).isBack()) {
    		portletSession.setAttribute("viewBean", new ViewBean());
    	} else {
    		if (((ViewBean)portletSession.getAttribute("viewBean")).isProcessing() == false) {
    			if ( ((ViewBean)portletSession.getAttribute("viewBean")).isValidAll() == false) { 
    				portletSession.setAttribute("viewBean", new ViewBean());
    			}
    		} else {
    			((ViewBean)portletSession.getAttribute("viewBean")).setProcessing(false);
    		}
    	}
    	
    	renderRequest.setAttribute("viewBean", (ViewBean)portletSession.getAttribute("viewBean"));
        include(viewJSP, renderRequest, renderResponse);
    }

    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }

}
