package ru.admomsk.server.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;

import com.liferay.util.TextFormatter;

import ru.admomsk.utils.MyUtils;

public class EditBean implements Serializable {
	
	private static final long serialVersionUID = 820105662596946556L;
	
	private PortletPreferences portletPreferences;
	private String to = "";
	private String from = "";
	private String passFrom = "";
    private String host = "";
    private String port = "";
    private String maxSizeAttach = "";
    private String formatAttach = "";
    private String adress = "";
    private String urlVerify = "";
    private String siteKey = "";
    private String secretKey = "";
    private String ip = "";
	
    private String validHost = "";
    private String validPort = "";
    private String validTo = "";
	private String validFrom = "";
	private String validMaxSizeAttach = "";
	
	private String badPort = "bad_port";
	private String badEmail = "bad_email";
	private String idle = "idle";
	private String ok = "ok";
	private String badMaxSizeAttach = "bad_max_size_attach";
    
	private boolean validAll;
	
	//private double maxSizeAttachInDouble = 0.0;
	
    public PortletPreferences getPortletPreferences() {
		return portletPreferences;
	}
	public void setPortletPreferences(PortletPreferences portletPreferences) {
		this.portletPreferences = portletPreferences;
	}
	public String getTo() throws ReadOnlyException {
		to = (String)portletPreferences.getValue("to", "");
		return to;
	}
	public void setTo(String to) throws ReadOnlyException, ValidatorException, IOException {
		this.to = to;
		portletPreferences.setValue("to", this.to);
		portletPreferences.store();
	}
	public String getFrom() {
		from = (String)portletPreferences.getValue("from", "");
		return from;
	}
	public void setFrom(String from) throws ReadOnlyException, ValidatorException, IOException {
		this.from = from;
		portletPreferences.setValue("from", this.from);
		portletPreferences.store();
	}
	public String getPassFrom() {
		passFrom = (String)portletPreferences.getValue("passFrom", "");
		return passFrom;
	}
	public void setPassFrom(String passFrom) throws ReadOnlyException, ValidatorException, IOException {
		this.passFrom = passFrom;
		portletPreferences.setValue("passFrom", this.passFrom);
		portletPreferences.store();
	}
	public String getHost() {
		host = (String)portletPreferences.getValue("host", "");
		return host;
	}
	public void setHost(String host) throws ReadOnlyException, ValidatorException, IOException {
		this.host = host;
		portletPreferences.setValue("host", this.host);
		portletPreferences.store();
	}
	public String getPort() {
		port = (String)portletPreferences.getValue("port", "");
		return port;
	}
	public void setPort(String port) throws ReadOnlyException, ValidatorException, IOException {
		this.port = port;
		portletPreferences.setValue("port", this.port);
		portletPreferences.store();
	}
	public String getMaxSizeAttach() {
		maxSizeAttach = (String)portletPreferences.getValue("maxSizeAttach", "");
		return maxSizeAttach;
	}
	public void setMaxSizeAttach(String maxSizeAttach) throws ReadOnlyException, ValidatorException, IOException {
		this.maxSizeAttach = maxSizeAttach;
		portletPreferences.setValue("maxSizeAttach", this.maxSizeAttach);
		portletPreferences.store();
	}
	public String getFormatAttach() {
		formatAttach = (String)portletPreferences.getValue("formatAttach", "");
		return formatAttach;
	}
	public void setFormatAttach(String formatAttach) throws ReadOnlyException, ValidatorException, IOException {
		this.formatAttach = formatAttach.trim();
		portletPreferences.setValue("formatAttach", this.formatAttach);
		portletPreferences.store();
	}
	public String getAdress() {
		adress = (String)portletPreferences.getValue("adress", "");
		return adress;
	}
	public void setAdress(String adress) throws ReadOnlyException, ValidatorException, IOException {
		String a;
		if (adress == null) {
			a = "";
		} else {
			a = adress;
		}
		
		this.adress = a.trim();
		portletPreferences.setValue("adress", this.adress);
		portletPreferences.store();
	}
	public String getUrlVerify() {
		urlVerify = (String)portletPreferences.getValue("urlVerify", "");
		return urlVerify;
	}
	public void setUrlVerify(String urlVerify) throws ReadOnlyException, ValidatorException, IOException {
		String uv;
		if (urlVerify == null) {
			uv = "";
		} else {
			uv = urlVerify;
		}
		
		this.urlVerify = uv;
		portletPreferences.setValue("urlVerify", this.urlVerify);
		portletPreferences.store();
		
	}
	public String getSiteKey() {
		siteKey = (String)portletPreferences.getValue("siteKey", "");
		return siteKey;
	}
	public void setSiteKey(String siteKey) throws ReadOnlyException, ValidatorException, IOException {
		String sk;
		if (siteKey == null) {
			sk = "";
		} else {
			sk = siteKey;
		}
		
		this.siteKey = sk;
		portletPreferences.setValue("siteKey", this.siteKey);
		portletPreferences.store();
		
	}
	public String getSecretKey() {
		secretKey = (String)portletPreferences.getValue("secretKey", "");
		return secretKey;
	}
	public void setSecretKey(String secretKey) throws ReadOnlyException, ValidatorException, IOException {
		String sk;
		if (secretKey == null) {
			sk = "";
		} else {
			sk = secretKey;
		}
		
		this.secretKey = sk;
		portletPreferences.setValue("secretKey", this.secretKey);
		portletPreferences.store();
		
	}
	public String getIp() {
		ip = (String)portletPreferences.getValue("ip", "");
		return ip;
	}
	public void setIp(String ip) throws ReadOnlyException, ValidatorException, IOException {
		String i;
		if (ip == null) {
			i = "";
		} else {
			i = ip;
		}
		
		this.ip = i.trim();
		portletPreferences.setValue("ip", this.ip);
		portletPreferences.store();
	}
	
	public String getValidHost() {
		return validHost;
	}
	public void validateHost() {
		if (host.equals("")) validHost = idle;
		else validHost = ok;
	}
	public String getValidPort() {
		return validPort;
	}
	public void validatePort() {
		if (port.equals("")) validPort = idle;
		else if (!MyUtils.doMatch(port, MyUtils.ONLY_DIGITS)) validPort = badPort;
		else validPort = ok;
	}
	public String getValidTo() {
		return validTo;
	}
	public void validateTo() {
		if (to.equals("")) validTo = idle;
		else if (!MyUtils.doMatch(to, MyUtils.EMAIL)) validTo = badEmail;
		else validTo = ok;
	}
	public String getValidFrom() {
		return validFrom;
	}
	public void validateFrom() {
		if (from.equals("")) validFrom = idle;
		else if (!MyUtils.doMatch(from, MyUtils.EMAIL)) validFrom = badEmail;
		else validFrom = ok;
	}
	public String getValidMaxSizeAttach() {
		return validMaxSizeAttach;
	}
	public void validateMaxSizeAttach() {
		if (maxSizeAttach.equals("")) validMaxSizeAttach = idle;
		else if (!MyUtils.doMatch(maxSizeAttach, MyUtils.ONLY_DIGITS_PLUS_DOT)) validMaxSizeAttach = badMaxSizeAttach;
		else validMaxSizeAttach = ok;
	}
	
	public String getBadPort() {
		return badPort;
	}
	public void setBadPort(String badPort) {
		this.badPort = badPort;
	}
	public String getBadEmail() {
		return badEmail;
	}
	public void setBadEmail(String badEmail) {
		this.badEmail = badEmail;
	}
	public String getIdle() {
		return idle;
	}
	public void setIdle(String idle) {
		this.idle = idle;
	}
	public String getOk() {
		return ok;
	}
	public void setOk(String ok) {
		this.ok = ok;
	}
	public String getBadMaxSizeAttach() {
		return badMaxSizeAttach;
	}
	public void setBadMaxSizeAttach(String badMaxSizeAttach) {
		this.badMaxSizeAttach = badMaxSizeAttach;
	}
	public boolean isValidAll() throws ReadOnlyException {
		getHost();
		validateHost();
		
		getPort();
		validatePort();
		
		getTo();
		validateTo();
		
		getFrom();
		validateFrom();
		
		getMaxSizeAttach();
		validateMaxSizeAttach();
		
		if (
			validHost.equals(ok) &&
			validPort.equals(ok) &&
			validTo.equals(ok) && 
			validFrom.equals(ok) && 
			validMaxSizeAttach.equals(ok)
		) {
			validAll = true;
		} else {
			System.out.println("validHost = "+validHost);
			System.out.println("validPort = "+validPort);
			System.out.println("validTo = "+validTo);
			System.out.println("validFrom = "+validFrom);
			System.out.println("validMaxSizeAttach = "+validMaxSizeAttach);
			validAll = false;
		}
		
		return validAll;
	}
	public void setValidAll(boolean validAll) {
		this.validAll = validAll;
	}
	public double getMaxSizeAttachInDouble() {
		return Double.valueOf(getMaxSizeAttach()).doubleValue()*1024.0*1024.0;
	}
    
}
