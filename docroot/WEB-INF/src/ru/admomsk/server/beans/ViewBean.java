package ru.admomsk.server.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;

import javax.portlet.ReadOnlyException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.FileUtil;

import ru.admomsk.utils.MyUtils;
import ru.admomsk.utils.SendMailUsage;

public class ViewBean implements Serializable {
	
	private static final long serialVersionUID = 7058165637422514755L;
	
	public static String SENT_USER_SUCC = "sent_user_succ";
	public static String SENT_USER_FAIL = "sent_user_fail";
	public static String SENT_SED_SUCC = "sent_sed_succ";
	public static String SENT_SED_FAIL = "sent_sed_fail";
	private String badEmail = "bad_email";
	private String idle = "idle";
	private String ok = "ok";
	private String badSizeAttach = "bad_size_attach";
	private String badFormatAttach = "bad_format_attach";
	
	private String depCode = ""; 
	private String surname = "";
	private String firstname = "";
	private String middlename = "";
	private String index = "";
	private String city = "";
	private String street = "";
	private String home = "";
	private String housing = "";
	private String apartment = "";
	private String organization = "";
	private String numberPhone = "";
	private String email = "";
	private String textAppeal = "";
	private String reCaptcha = "";
	
	private File attach = null;
	private File tempAttach = null;
	private File requestDir = null;
	private EditBean editBean;
	private String viewURL;
	private String renderURL = "";
	private String realAttachName;
	private String regexFormatAttach = ", ";
	
	private String validDepCode = ok;
	private String validSurname = ok;
	private String validFirstname = ok;
	private String validEmail = ok;
	private String validTextAppeal = ok;
	private String validReCaptcha = ok;
	private String validAttach = ok;
	
	private boolean validAll = true;
	private boolean sent = false;
	private String sentStatus = "";
	private boolean viewForm = true;
	private boolean back = false;
	private boolean processing = false;
	/*private boolean editValidAll = false;*/
	
	
	private Exception exception = null;
	private String localizedMessage = "";
	/*
	public String NOT_EMAIL = "not_email";
	public String EMPTY = "empty";
	public String OK = "ok";
	*/
	
	public String getDepCode() {
		return depCode;
	}
	
	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		if (surname == null) this.surname = "";
		else this.surname = surname;
	}
	
	public String getFirstname() {
		return firstname;
	}
	
	public void setFirstname(String firstname) {
		if (firstname == null) this.firstname = "";
		else this.firstname = firstname; 
		
	}
	
	public String getMiddlename() {
		return middlename;
	}
	
	public void setMiddlename(String middlename) {
		if (middlename == null) this.middlename = "";
		else this.middlename = middlename;
		
	}
	
	public String getIndex() {
		return index;
	}
	
	public void setIndex(String index) {
		if (index == null) this.index = "";
		else this.index = index;
		
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		if (city == null) this.city = "";
		else this.city = city;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		if (street == null) this.street = "";
		else this.street = street;
		
	}
	
	public String getHome() {
		return home;
	}
	
	public void setHome(String home) {
		if (home == null) this.home = "";
		else this.home = home;
	}
	
	public String getHousing() {
		return housing;
	}
	
	public void setHousing(String housing) {
		if (housing == null) this.housing = "";
		else this.housing = housing;
	}
	
	public String getApartment() {
		return apartment;
	}
	
	public void setApartment(String apartment) {
		if (apartment == null) this.apartment = "";
		else this.apartment = apartment;
	}
	
	public String getOrganization() {
		return organization;
	}
	
	public void setOrganization(String organization) {
		if (organization == null) this.organization = "";
		else this.organization = organization;
	}
	
	public String getNumberPhone() {
		return numberPhone;
	}
	
	public void setNumberPhone(String numberPhone) {
		if (numberPhone == null) this.numberPhone = "";
		else this.numberPhone = numberPhone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		if (email == null) this.email = "";
		else this.email = email;
	}
	
	public String getTextAppeal() {
		return textAppeal;
	}
	
	public void setTextAppeal(String textAppeal) {
		if (textAppeal == null) this.textAppeal = "";
		else this.textAppeal = textAppeal;
	}
	
	public String getReCaptcha() {
		return reCaptcha;
	}

	public void setReCaptcha(String reCaptcha) {
		if (reCaptcha == null) this.reCaptcha = "";
		else this.reCaptcha = reCaptcha;
	}

	public File getAttach() {
		return attach;
	}
	
	public void setAttach(File attach, String realAttachName) {
		this.tempAttach = attach;
		this.attach = attach;
		this.realAttachName = realAttachName;
	}
	
	public String getValidDepCode() {
		return validDepCode;
	}
	public void validateDepCode() {
		if (depCode.equals("")) validDepCode = idle;
		else validDepCode = ok;
	}
	
	public String getValidSurname() {
		return validSurname;
	}
	
	public void validateSurname() {
		if (surname.equals("")) validSurname = idle;
		else validSurname = ok;
	}
	
	public String getValidFirstname() {
		return validFirstname;
	}
	
	public void validateFirstname() {
		if (firstname.equals("")) validFirstname = idle;
		else validFirstname = ok;
	}
	
	public String getValidEmail() {
		return validEmail;
	}
	
	public void validateEmail() {
		if (email.equals("")) validEmail = idle;
		else if (!MyUtils.doMatch(email, MyUtils.EMAIL)) validEmail = badEmail;
		else validEmail = ok;
	}
	
	public String getValidTextAppeal() {
		return validTextAppeal;
	}
	
	public void validateTextAppeal() {
		if (textAppeal.equals("")) validTextAppeal = idle;
		else validTextAppeal = ok;
	}
	
	public String getValidReCaptcha() {
		return validReCaptcha;
	}
	
	public void validateReCaptcha(String ipRemote) {
		boolean isUrlVerify = editBean != null && editBean.getUrlVerify() != null && !editBean.getUrlVerify().isEmpty();
		boolean isSiteKey = editBean != null && editBean.getSiteKey() != null && !editBean.getSiteKey().isEmpty();
		boolean isSecretKey = editBean != null && editBean.getSecretKey() != null && !editBean.getSecretKey().isEmpty();
		boolean isReCaptcha = reCaptcha != null && !reCaptcha.equals("");
		boolean isVerify = false;
		boolean isValidate = false;
		
		//System.out.println("1 isUrlVerify = " + isUrlVerify + " isSiteKey = " + isSiteKey + " isSecretKey = " + isSecretKey + " isReCaptcha = " + isReCaptcha + " isVerify = " + isVerify + " isValidate = " + isValidate);
		
		if (isUrlVerify && isSiteKey && isSecretKey) {
			isValidate = true;
			if (isReCaptcha) {
				isVerify = siteVerify(editBean.getUrlVerify(), reCaptcha, editBean.getSecretKey(), ipRemote);
			}
		} else {
			isValidate = false;
		}
		
		//System.out.println("2 isUrlVerify = " + isUrlVerify + " isSiteKey = " + isSiteKey + " isSecretKey = " + isSecretKey + " isReCaptcha = " + isReCaptcha + " isVerify = " + isVerify + " isValidate = " + isValidate);
		
		if (isValidate) {
		
			if (isVerify) {
				validReCaptcha = ok;
			} else {
				validReCaptcha = idle;
			}
		} else {
			validReCaptcha = ok;
		}
	}
	
	private boolean siteVerify(String urlVerify, String reCaptchaResponse, String secretKey, String ipRemote) {
		
		if (
				urlVerify == null || urlVerify.equals("") || 
				reCaptchaResponse == null || reCaptchaResponse.equals("") ||
				secretKey == null || secretKey.equals("")) {
			return false;
		}
		
		boolean isSuccess = false;
		
		try {
			
			HttpClient client = new HttpClient();
			PostMethod postMethod = new PostMethod(urlVerify);
			postMethod.addParameter("response", reCaptchaResponse);
			postMethod.addParameter("secret", secretKey);
			
			boolean isIp = !editBean.getIp().equals("");
			if (ipRemote != null && !ipRemote.equals("") && isIp) {
				postMethod.addParameter("remoteip", ipRemote);
			}
			
			// Execute the method.
			int statusCode = client.executeMethod(postMethod);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("post siteverify failed: " + postMethod.getStatusLine());
				return false;
			}
			// Read the response body.
			byte[] responseBody = postMethod.getResponseBody();
	
			// Deal with the response.
			// Use caution: ensure correct character encoding and is not binary data
			String responseStr = new String(responseBody);  
			
			JSONObject json = JSONFactoryUtil.createJSONObject(responseStr);
			
			isSuccess = json.getBoolean("success");
		
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isSuccess = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isSuccess = false;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isSuccess = false;
		}
		
		return isSuccess;
	}
	
	public String getValidAttach() {
		return validAttach;
	}
	
	public void setValidAttach(String validAttach) {
		this.validAttach = validAttach;
	}
	
	public void validateAttach() {
		if (tempAttach == null) return;
        
		byte[] bytes = null;
		try {
			bytes = FileUtil.getBytes(tempAttach);
			
			//проверка на формат
			if (!editBean.getFormatAttach().equals("")) {
	            if (!MyUtils.findExtInRow(MyUtils.getExtNotDot(realAttachName), editBean.getFormatAttach(), regexFormatAttach)){
	            	validAttach = badFormatAttach;
	            	return;
	            } else validAttach = ok;
            } else validAttach = ok;
			
			//проверка на размер
            if (Double.valueOf(String.valueOf(bytes.length)) > Double.valueOf(editBean.getMaxSizeAttach())*1024.0*1024.0) {
            	validAttach = badSizeAttach;
            	return;
            } else validAttach = ok;
            
            
			//создать временную папку для request.xml и для attach
			createReqDir();
			
			
        } catch (IOException e) {
        	exception = e;
            e.printStackTrace();
        }
        
        if ((bytes != null) && (bytes.length > 0)) {
        	
        	FileInputStream fileInputStream = null;
	        FileOutputStream fileOutputStream = null; 
	        try {
	        	attach = new File(requestDir, realAttachName);
		        fileInputStream = new FileInputStream(tempAttach);
		        fileOutputStream = new FileOutputStream(attach);            
		        fileInputStream.read(bytes);                
		        fileOutputStream.write(bytes, 0, bytes.length);
		        fileOutputStream.flush();
		        fileOutputStream.close();
		        fileInputStream.close();
	        } catch (IOException e) {
	        	exception = e;
	            e.printStackTrace();
	        } finally {
	        	try{
		        	fileOutputStream.close();
			        fileInputStream.close();
			        FileUtils.forceDelete(tempAttach);
			        /*if (!tempAttach.delete()){
			        	tempAttach.delete();
			        }*/
	        	} catch (Exception e){
	        		exception = e;
	        		e.printStackTrace();
	        	}
	        }
	        
        
        }
    }
	
	public void removeAttach() {
		
		try {
			if (tempAttach != null) {
				if (tempAttach.exists()) {
					FileUtils.forceDelete(tempAttach);
				}
			}
			if (attach != null) {
				if (attach.exists()) {
					FileUtils.forceDelete(attach);
				}
			}
			realAttachName = null;

			if (requestDir != null) {
				if (requestDir.exists()) {
					FileUtils.deleteDirectory(requestDir);
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
    
	public String getBadEmail() {
		return badEmail;
	}
	/*public void setNOT_EMAIL(String nOT_EMAIL) {
		NOT_EMAIL = nOT_EMAIL;
	}*/
	public String getIdle() {
		return idle;
	}
	/*public void setEMPTY(String eMPTY) {
		EMPTY = eMPTY;
	}*/
	public String getOk() {
		return ok;
	}
	/*public void setOK(String oK) {
		OK = oK;
	}*/
	public String getBadSizeAttach() {
		return badSizeAttach;
	}
	public String getBadFormatAttach() {
		return badFormatAttach;
	}
	public void setLocalizedMessage(String localizedMessage) {
		this.localizedMessage = localizedMessage;
	}
	public String getLocalizedMessage() {
		if (exception != null) {
			localizedMessage = exception.getLocalizedMessage();
		}
		return localizedMessage;
	}
	

	public boolean isValidAll() {
		if (
			validDepCode.equals(ok) &&
			validSurname.equals(ok) && 
			validFirstname.equals(ok) && 
			/*
			validIndex.equals(ok) && 
			validCity.equals(ok) && 
			validStreet.equals(ok) &&
			validHome.equals(ok) &&
			*/
			//Квартиру закомментировать, возможно пригодится
			//validApartment.equals(ok) &&
			validEmail.equals(ok) &&
			validTextAppeal.equals(ok) &&
			validReCaptcha.equals(ok) &&
			validAttach.equals(ok)
		) {
			validAll = true;
		} else {
			validAll = false;
		}
		
		return validAll;
	}
	
	public void resetValid() {
		validDepCode = "ok";
		validSurname = "ok";
		validFirstname = "ok";
		validEmail = "ok";
		validTextAppeal = "ok";
		validReCaptcha = "ok";
		validAttach = "ok";
		validAll = true;
	}
	
	public void resetValue() {
		depCode = "";
		surname = "";
		firstname = "";
		middlename = "";
		index = "";
		city = "";
		street = "";
		home = "";
		housing = "";
		apartment = "";
		numberPhone = "";
		email = "";
		textAppeal = "";
		reCaptcha = "";
		attach = null;
		tempAttach = null;
		requestDir = null;
	}
	
	public boolean isSent() {
		return sent;
	}
	
	public void setSent(boolean sent) {
		this.sent = sent;
	}
	
	public String getSentStatus() {
		return sentStatus;
	}
	
	public void setSentStatus(String sentStatus) {
		this.sentStatus = sentStatus;
	}
	
	public boolean isViewForm() {
		return viewForm;
	}
	
	public void setViewForm(boolean viewForm) {
		this.viewForm = viewForm;
	}

	public boolean isBack() {
		return back;
	}

	public void setBack(boolean back) {
		this.back = back;
	}
	
	public boolean isProcessing() {
		return processing;
	}

	public void setProcessing(boolean processing) {
		this.processing = processing;
	}
	
	public EditBean getEditBean() {
		return editBean;
	}

	public void setEditBean(EditBean editBean) {
		this.editBean = editBean;
	}
	
	@SuppressWarnings("finally")
	public boolean sendMessageToUser() {
		boolean result = false;
		SendMailUsage sendMailUsage = new SendMailUsage();
		
		//от кого
		sendMailUsage.setFrom(editBean.getFrom());
		sendMailUsage.setPassword(editBean.getPassFrom());
		//кому
		sendMailUsage.setTo(this.email);
		sendMailUsage.setHost(editBean.getHost());
		sendMailUsage.setPort(Integer.valueOf(editBean.getPort()));
		sendMailUsage.setUsername("Portal");
		sendMailUsage.setSubject("Обращение в Администрацию города Омска: завершение процедуры");
		
		
		//Создать request.xml
		PrintWriter pw = null;
		//Если requestDir пустой, то создать его
		if (requestDir == null) createReqDir();
		//Если requestDir не существует, то создать его
		if (!requestDir.exists()) createReqDir();
		//создать request.xml
		File request = new File(requestDir, "request.xml");
		
		
		try {
			
			//пустым сделать "null"
			if (index.isEmpty()) {
				index = "null";
			} 
			
			if (city.isEmpty()) {
				city = "null";
			}
			
			if (street.isEmpty()) {
				street = "null";
			}
			
			if (home.isEmpty()) {
				home = "null";
			}
			
			if (housing.isEmpty()) {
				housing = "null";
			} 
			
			if (apartment.isEmpty()) {
				apartment = "null";
			}
			
			pw = new PrintWriter(request, "utf-8");
			
			StringBuilder sb = new StringBuilder();
			/*Записать данные request xml*/
			pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			pw.println("<UserQuestion xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"UserRequests.xsd\" >");
			pw.println("<login>" + StringEscapeUtils.escapeXml(email) + "</login>");
			pw.println("<nameLast>" + StringEscapeUtils.escapeXml(surname) + "</nameLast>");
			pw.println("<nameFirst>" + StringEscapeUtils.escapeXml(firstname) + "</nameFirst>");
			pw.println("<nameMiddle>" + StringEscapeUtils.escapeXml(middlename) + "</nameMiddle>");
			pw.println("<passport>" + "null" + "</passport>"); 
			pw.println("<phone>" + StringEscapeUtils.escapeXml(numberPhone) + "</phone>");
			pw.println("<state>" + "null" + "</state>");
			pw.println("<region>" + "null" + "</region>");
			pw.println("<city>" + StringEscapeUtils.escapeXml(city) + "</city>");
			pw.println("<locality>" + "null" + "</locality>");
			pw.println("<street>" + StringEscapeUtils.escapeXml(street) + "</street>");
			pw.println("<house>" + StringEscapeUtils.escapeXml(home) + "</house>");
			pw.println("<corp>" + StringEscapeUtils.escapeXml(housing) + "</corp>");
			pw.println("<flat>" + StringEscapeUtils.escapeXml(apartment) + "</flat>");
			pw.println("<zipCode>" + StringEscapeUtils.escapeXml(index) + "</zipCode>");
			String textOrg = "";
			if (organization != null && !organization.equals("")) {
				String quote = "\"";
				textOrg = quote.concat("От организации ").concat(organization).concat(quote).concat(" ");
			}
			pw.println("<question>" + StringEscapeUtils.escapeXml(textOrg + textAppeal) + "</question>");
			pw.println("<depCode>" + StringEscapeUtils.escapeXml(depCode) + "</depCode>");
			if (attach != null) {
				pw.println("<file>" + StringEscapeUtils.escapeXml(attach.getName()) + "</file>");
			}
			pw.println("</UserQuestion>");
			
			pw.println();
			pw.close();
			
			String sendUrl = renderURL.replaceAll("i_m_id=null", "i_m_id=" + requestDir.getName());
			
			StringBuilder letterToUser = new StringBuilder();
			
			letterToUser.append("<p>Здравствуйте, ").append(firstname);
			if (!middlename.equals("")) letterToUser.append(" ").append(middlename);
			letterToUser.append(".").append("</p>");
			letterToUser.append("<p>Вы написали обращение в Администрацию города Омска. Чтобы завершить отправку обращения, пожалуйста, выполните переход по ссылке:").append("</p>");
			
			if (sendUrl != null) {
				if (sendUrl.equals("")) {
					letterToUser.append("<p>").append("<b>Ссылка для вашего обращения по каким-то причинам не сформировалась. Ваше обращение не отправлено. Попробуйте отправить еще раз, либо обратитесь в поддержку портала Администрации города Омска.</b>").append("</p>");
				} else {
					//если всё корректно
					letterToUser.append("<p><a href=\"").append(sendUrl).append("\">").append(sendUrl).append("</a>").append("</p>");
				}
			} else {
				letterToUser.append("<p>").append("<b>Ссылка для вашего обращения по каким-то причинам не сформировалась. Ваше обращение не отправлено. Попробуйте отправить еще раз, либо обратитесь в поддержку портала Администрации города Омска.</b>").append("</p>");
			}
			
			letterToUser.append("<p>Этим переходом вы подтверждаете работоспособность вашего адреса электронной почты. ");
			letterToUser.append("Отправка вашего обращения будет завершена <b>только после перехода по ссылке</b>.").append("</p>");
			letterToUser.append("<p>Текст обращения:").append("<br>");
			letterToUser.append("================================").append("<br>");
			letterToUser.append(textAppeal).append("<br>");
			letterToUser.append("================================").append("</p>");
			letterToUser.append("<p>С уважением,").append("<br>");
			letterToUser.append("команда поддержки официального портала Администрации города Омска.</p>");
			
			sendMailUsage.setText(letterToUser.toString());
			
			result = sendMailUsage.sendMessageToUser();
			
			if (sendMailUsage.getException() != null) {
				localizedMessage = sendMailUsage.getException().getLocalizedMessage();
				System.out.println("sendMailUsage.getException() = "+sendMailUsage.getException().getLocalizedMessage());
			}
			
		}
		catch(Exception e) {
			exception = e;
			e.printStackTrace();
		} finally {
			return result;
		}
		
	}
	
	@SuppressWarnings({ "finally", "unchecked" })
	public boolean sendMessageToSed(String nameDir) throws ReadOnlyException {
		boolean result = false;
		
		requestDir = new File(System.getProperty("java.io.tmpdir") + File.separator + nameDir);
		//если папка не существует, то завершить отправку
		if (!requestDir.exists()) return result;
		
		
		SendMailUsage sendMailUsage = new SendMailUsage();
		ArrayList<File> alAttach = null;
		sendMailUsage.setFrom(editBean.getFrom());
		sendMailUsage.setPassword(editBean.getPassFrom());
		sendMailUsage.setTo(editBean.getTo());
		sendMailUsage.setHost(editBean.getHost());
		sendMailUsage.setPort(Integer.valueOf(editBean.getPort()));
		sendMailUsage.setUsername("Portal");
		sendMailUsage.setSubject("InteractiveMessages");
		sendMailUsage.setText("");
		
		File[] files = requestDir.listFiles();
		
		try {
			
			//Прикрепить request.xml
			alAttach = new ArrayList<File>();
			
			for(int i = 0; i < files.length; i++) {
				alAttach.add(files[i]);
			}
			
			sendMailUsage.setFileAsAttachment(alAttach);
			result = sendMailUsage.sendMessageToSed();
			if (sendMailUsage.getException() != null) {
				localizedMessage = sendMailUsage.getException().getLocalizedMessage();
				System.out.println("sendMailUsage.getException() = "+sendMailUsage.getException().getLocalizedMessage());
			}
			
		}
		catch(Exception e) {
			exception = e;
			e.printStackTrace();
		} finally {
			//переименовать в папка_activated
			//File requestDirActivated = new File(requestDir.getAbsolutePath() + "_activated");
			
			if (requestDir != null) {
				if (requestDir.exists()) {
					
					/*
					FileUtil.move(requestDir, requestDirActivated);
					
					//обновить alAttach
					alAttach = new ArrayList<File>();
					for (File f:(LinkedList<File>)FileUtils.listFiles(requestDirActivated, null, false)) {
						alAttach.add(f);
					}*/
					
					//Удалить папку
					//FileUtil.deltree(requestDirActivated);
					FileUtil.deltree(requestDir);
				}
			}
			
			//проверить еще раз папку и удалить ее в случае необходимости
			try {
				//старый способ удаления
				//File requestDirActivated = new File(requestDir.getAbsolutePath() + "_activated");
				/*FileUtil.
				for (File f:alAttach) {
					FileUtils.moveFileToDirectory(requestDir, requestDirActivated, true);
				}*/
				//requestDir.renameTo(requestDirActivated);
				//FileUtils.moveDirectoryToDirectory(requestDir, requestDirActivated, true);
				
				//удалить файлы
				/*if (alAttach != null) {
					for (File f:alAttach) {
						FileUtils.forceDelete(f);
					}
				}
				alAttach = null;*/
				
				//удалить папку
				/*if (requestDirActivated != null) {
					if (requestDirActivated.exists()) {
						FileUtils.deleteDirectory(requestDirActivated);
					}
				}*/
				if (requestDir != null) {
					if (requestDir.exists()) {
						FileUtils.deleteDirectory(requestDir);
					}
				}
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return result;
		}
	}
	
	public String getViewURL() {
		return viewURL;
	}
	
	public void setViewURL(String viewURL) {
		this.viewURL = viewURL;
	}
	
	public String getRenderURL() {
		return renderURL;
	}
	
	public void setRenderURL(String renderURL) {
		this.renderURL = renderURL;
	}
	
	//создать папку requestDir
	private void createReqDir() {
		//Создать временный файл tempFile(во временной директории)
		File tempFile;
		try {
			tempFile = File.createTempFile("interactive_messages", "");
			//Получить path временного файла
			String pathTempFile = tempFile.getAbsolutePath();
			//Удалить временный файл
			FileUtils.forceDelete(tempFile);
			//создать директорию на основе path временного файла
			requestDir = new File(pathTempFile);
			FileUtils.forceMkdir(requestDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
