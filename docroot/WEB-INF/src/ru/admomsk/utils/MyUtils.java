package ru.admomsk.utils;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyUtils {
	
	public static String ONLY_STRING_AND_MINUS_SPACE = "([а-яА-Я-\\u0020]+)";
	public static String ONLY_STRING = "([а-яА-Я]+)";
	public static String EMAIL = "^([a-zA-Z0-9_\\.\\-+])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$";
	public static String ONLY_DIGITS = "([0-9]+)";
	/*по мотивам http://crypto.pp.ua/2010/06/regulyarnye-vyrazheniya-java/ */
	//public static String ONLY_DIGITS_PLUS_DOT = "([0-9+]\\.?[0-9+])";
	public static String ONLY_DIGITS_PLUS_DOT = "([0-9])+(\\.([0-9])+)?";
	
	public static boolean doMatch(String word, String regex) {
		Pattern patternCompile = Pattern.compile(regex);
        Matcher matcher = patternCompile.matcher(word);
        if (matcher.matches()){
        	return true;
        }

        return false;
    }
	
	
	//Получить имя файла без расширения
	public static String getFileName(String filename) {
		//Получить позицию точки в файле
		int dotPos = filename.lastIndexOf(".");
		//Если нет точки, то вернуть имя полностью (значит файл без расширения)
		if (dotPos == -1) return filename; 
		//Имя файла до точки
		return filename.substring(0, dotPos);
	}
	
	//Получить расширение файла с точкой
	public static String getExt(String filename) {
		int dotPos = filename.lastIndexOf(".");
		if (dotPos == -1) return null; 
		//Расширение с точкой
		return filename.substring(dotPos);
	}
	
	//Получить расширение файла без точки
	public static String getExtNotDot(String filename) {
		int dotPos = filename.lastIndexOf(".");
		if (dotPos == -1) return null; 
		//Расширение с точкой
		return filename.substring(dotPos+1);
	}
	
	//Искать расширение в строке, не чувствительно к регистру
	public static boolean findExtInRow(String ext, String row, String regex) {
		String[] exts = row.split(regex);
		for (int i=0; i<exts.length; i++) {
			if (ext.equalsIgnoreCase(exts[i])) return true;
		}
		return false;
	}
	
	/**
	* Deletes directory with subdirs and subfolders
	* @author Cloud
	* @param dir Directory to delete
	*/
	public static void deleteDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i=0; i<children.length; i++) {
				File f = new File(dir, children[i]);
				deleteDirectory(f);
			}
			dir.delete();
		} else dir.delete();
	}
}
