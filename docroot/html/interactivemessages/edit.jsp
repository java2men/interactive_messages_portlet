<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<%@ page import="ru.admomsk.server.InteractiveMessages" %>

<portlet:defineObjects />
<jsp:useBean id="editBean" class="ru.admomsk.server.beans.EditBean" scope="request"/>

<form id="form_edit_inter_mess" name="form_edit_inter_mess" enctype="application/x-www-form-urlencoded" action="<portlet:actionURL />" method="post">
	
	<table >
		<tbody>
			
			<tr>
				
				<c:set var="login" value="Бендер" scope="page"/>
				
				<td class="aqLabelColumn">
					<table>
						<tbody>
							<tr>
								<td>E-mail адрес почтового ящика СЭД:</td>
								<td>
									<span class="aqRequired">*</span>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td class=" aqValueColumn">
					<input id="email_sed_inter_mess" name="email_sed_inter_mess" type="text" size="30" value="<c:out value="${editBean.to}"></c:out>" >
					<div class="aqInfoMessage">(например, PortalGateway@admomsk.ru)</div>
					<c:if test="${editBean.validTo eq editBean.idle}">
	  						<div class="aqErrorMessage">Поле обязательно для заполнения.</div>
					</c:if>
					<c:if test="${editBean.validTo eq editBean.badEmail}">
	  						<div class="aqErrorMessage">Адрес электронной почты введен некорректно.</div>
					</c:if>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">
					<table>
						<tbody>
							<tr>
								<td>SMPT:</td>
								<td>
									<span class="aqRequired">*</span>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				
				<td class=" aqValueColumn">
					<input id="email_smpt_inter_mess" name="email_smpt_inter_mess" type="text" size="30" value="<c:out value="${editBean.host}"></c:out>" >
					<div class="aqInfoMessage">(например, 217.25.215.29)</div>
					<c:if test="${editBean.validHost eq editBean.idle}">
		  				<div class="aqErrorMessage">Поле обязательно для заполнения.</div>
					</c:if>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">
					<table>
						<tbody>
							<tr>
								<td>Порт:</td>
								<td>
									<span class="aqRequired">*</span>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				
				<td class=" aqValueColumn">
					<input id="email_port_inter_mess" name="email_port_inter_mess" type="text" size="30" value="<c:out value="${editBean.port}"></c:out>" >
					<div class="aqInfoMessage">(например, 25)</div>
					<c:if test="${editBean.validPort eq editBean.idle}">
		  				<div class="aqErrorMessage">Поле обязательно для заполнения.</div>
					</c:if>
					<c:if test="${editBean.validPort eq editBean.badPort}">
	  						<div class="aqErrorMessage">Порт должен содержать только цифры.</div>
					</c:if>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">
					<table>
						<tbody>
							<tr>
								<td>Email портала:</td>
								<td>
									<span class="aqRequired">*</span>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				
				<td class=" aqValueColumn">
					<input id="email_from_inter_mess" name="email_from_inter_mess" type="text" size="30" value="<c:out value="${editBean.from}"></c:out>" >
					<div class="aqInfoMessage">(например, noreply@admomsk.ru)</div>
					<c:if test="${editBean.validFrom eq editBean.idle}">
	  						<div class="aqErrorMessage">Поле обязательно для заполнения.</div>
					</c:if>
					<c:if test="${editBean.validFrom eq editBean.badEmail}">
	  						<div class="aqErrorMessage">Адрес электронной почты введен некорректно.</div>
					</c:if>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">Пароль email портала:</td>
				<td class=" aqValueColumn">
					<input id="pass_email_from_inter_mess" name="pass_email_from_inter_mess" type="password" size="30" value="<c:out value="${editBean.passFrom}"></c:out>" >
					<div class="aqInfoMessage">(может отсутствовать)</div>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">
					<table>
						<tbody>
							<tr>
								<td>Максимальный размер прикрепляемого файла:</td>
								<td>
									<span class="aqRequired">*</span>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				
				<td class=" aqValueColumn">
					<input id="max_size_attach_inter_mess" name="max_size_attach_inter_mess" type="text" size="30" value="<c:out value="${editBean.maxSizeAttach}"></c:out>" >
					<div class="aqInfoMessage">Размер указывается в Мбайт. </div>
					<c:if test="${editBean.validMaxSizeAttach eq editBean.idle}">
	  						<div class="aqErrorMessage">Поле обязательно для заполнения.</div>
					</c:if>
					<c:if test="${editBean.validMaxSizeAttach eq editBean.badMaxSizeAttach}">
	  						<div class="aqErrorMessage">Некорректны ввод. Допустимыми являются цифры и символ точки.</div>
					</c:if>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">Допустимые форматы прикрепляемого файла:</td>
				<td class=" aqValueColumn">
					<input id="format_attach_inter_mess" name="format_attach_inter_mess" type="text" size="30" value="<c:out value="${editBean.formatAttach}"></c:out>" >
					<div class="aqInfoMessage">Форматы указать через пробел с запятой (например, zip, pdf, doc, docx, jpg, jpeg, png, gif, bmp, tif, tiff), пустая строка означает - без ограничений. </div>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">Отображать адрес</td>
				<td class=" aqValueColumn">
					<input id="is_adress_inter_mess" name="is_adress_inter_mess" type="checkbox" value="checked" <c:out value="${editBean.adress}"></c:out>>
					<div class="aqInfoMessage">Выберите отображать или не отображать адрес. </div>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">URL верификации reCAPTCHA</td>
				<td class=" aqValueColumn">
					<input id="url_verify_inter_mess" name="url_verify_inter_mess" type="text" size="30" value="<c:out value="${editBean.urlVerify}"></c:out>" >
					<div class="aqInfoMessage">Например, URL: https://www.google.com/recaptcha/api/siteverify</div>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">Ключ сайта reCAPTCHA</td>
				<td class=" aqValueColumn">
					<input id="site_key_inter_mess" name="site_key_inter_mess" type="text" size="30" value="<c:out value="${editBean.siteKey}"></c:out>" >
					<div class="aqInfoMessage">Например, data-sitekey="6LcSa2AUAAAAAIalPo7qU-qST_vpXp8u9RmItPKO"</div>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">Секретный ключ reCAPTCHA</td>
				<td class=" aqValueColumn">
					<input id="secret_key_inter_mess" name="secret_key_inter_mess" type="text" size="30" value="<c:out value="${editBean.secretKey}"></c:out>" >
					<div class="aqInfoMessage">Например, secretkey="6LcSa2AUAAAAAPUsqVdHviu6P-DPVEqzODux3K5Y"</div>
				</td>
			</tr>
			
			<tr>
				<td class="aqLabelColumn">Запрашивать IP для reCAPTCHA</td>
				<td class=" aqValueColumn">
					<input id="is_ip_inter_mess" name="is_ip_inter_mess" type="checkbox" value="checked" <c:out value="${editBean.ip}"></c:out> >
					<div class="aqInfoMessage">Выберите запрашивать IP для reCAPTCHA или нет. </div>
				</td>
			</tr>
				
		</tbody>
	</table>	
	
	<table>
		<tbody>
			<tr>
				<td>
					<input id="save_edit_inter_mess" name="save_em_inter_mess"  type="submit" value="Сохранить">
				</td>
			</tr>
		</tbody>
	</table>
	
</form>
