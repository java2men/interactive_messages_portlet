<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<script src='https://www.google.com/recaptcha/api.js'></script>

<portlet:defineObjects />

<jsp:useBean id="errorSurname" class="java.lang.String" scope="request"/>
<jsp:useBean id="viewBean" class="ru.admomsk.server.beans.ViewBean" scope="request"/>
<portlet:renderURL var="viewUrl" portletMode="view"></portlet:renderURL>
<jsp:setProperty name="viewBean" property="viewURL" value='<%=viewUrl.toString()%>'/>

<c:if test="${not viewBean.editBean.validAll}">
	<div class="journal-content-article">
		<h2>Портлет не сконфигурирован!</h2>
		<div class="form-error-message">В настройках портлета имеются ошибки, исправьте их пожалуйста!</div>
	</div>
</c:if>

<c:if test="${viewBean.editBean.validAll}">
	
	<c:if test="${viewBean.viewForm}">
		<div id="kindergarden">
		<form id="form_send_question_inter_mess" name="form_send_question_inter_mess" enctype="multipart/form-data" action="<portlet:actionURL/>" method="post">
		<ul class="form-section">
			
			<li class="form-line form-line-column">	
				<label class="form-label-left">Место регистрации обращения<span class="form-required">*</span></label>	
				<div class="form-input-wide">	
					<span class="form-sub-label-container">	
						<select id="dep_code_inter_mess" name="dep_code_inter_mess" class="form-dropdown">	
							<option value="61">Администрация города Омска</option> 	
							<option value="2">Администрация Кировского административного округа</option> 	
							<option value="3">Администрация Ленинского административного округа</option> 	
							<option value="4">Администрация Октябрьского административного округа</option> 	
							<option value="5">Администрация Советского административного округа</option> 	
							<option value="6">Администрация Центрального административного округа</option> 	
							<option value="9">Департамент архитектуры и градостроительства</option> 	
							<option value="10">Департамент городского хозяйства</option> 	
							<option value="11">Департамент городской экономической политики</option> 	
							<option value="13">Департамент жилищной политики</option> 	
							<option value="15">Департамент имущественных отношений</option> 	
							<option value="30">Департамент информационной политики</option> 	
							<option value="42">Департамент контрактной системы в сфере закупок</option> 	
							<option value="16">Департамент культуры</option> 	
							<option value="17">Департамент образования</option> 	
							<option value="43">Департамент общественной безопасности</option> 	
							<option value="21">Департамент общественных отношений и социальной политики</option> 	
							<option value="25">Департамент по делам молодежи‚ физической культуры и спорта</option> 	
							<option value="20">Департамент правового обеспечения и муниципальной службы</option> 	
							<option value="22">Департамент строительства</option> 	
							<option value="23">Департамент транспорта</option> 	
							<option value="26">Департамент финансов и контроля</option> 	
							<option value="41">Управление административной практики и муниципального контроля</option> 	
							<option value="29">Управление делами</option> 	
						</select>	
						<label class="form-sub-label">Вы можете обратиться напрямую в профильное структурное подразделение</label>	
					</span>
					<c:if test="${viewBean.validDepCode eq viewBean.idle}">
					  	<div class="form-error-message">Пожалуйста, укажите место регистрации обращения.</div>
					</c:if>
				</div>
			</li>
			
			<li class="form-line form-line-column">
			
				<label class="form-label-left">Заявитель<span class="form-required">*</span></label>
				<div class="form-input-wide">
					
					<span class="form-sub-label-container">
						<input id="surname_inter_mess" name="surname_inter_mess" value='<c:out value="${viewBean.surname}"></c:out>' type="text" size="20" 
						class='<c:if test="${viewBean.validSurname eq viewBean.idle}">form-validation-error</c:if>'>
						<label class="form-sub-label">Фамилия</label>
					</span>
					<span class="form-sub-label-container">
						<input id="firstname_inter_mess" name="firstname_inter_mess" value='<c:out value="${viewBean.firstname}"></c:out>' type="text" size="15"
						class='<c:if test="${viewBean.validFirstname eq viewBean.idle}">form-validation-error</c:if>'>
						<label class="form-sub-label">Имя</label>
					</span>
					<span class="form-sub-label-container">
						<input id="middlename_inter_mess" name="middlename_inter_mess" value='<c:out value="${viewBean.middlename}"></c:out>' type="text" size="15">
						<label class="form-sub-label">Отчество</label>
					</span>
					<!--validation-->
			        <c:if test="${viewBean.validSurname eq viewBean.idle}">
					  	<div class="form-error-message">Пожалуйста, введите фамилию.</div>
					</c:if>
					<c:if test="${viewBean.validFirstname eq viewBean.idle}">
						<div class="form-error-message">Пожалуйста, введите имя.</div>
					</c:if>
		
				</div>
			</li>
			
			<c:if test="${viewBean.editBean.adress eq 'checked'}">
			
			
			<li class="form-line form-line-column">
				<label class="form-label-left">Адрес</label>
				<div class="form-input-wide">
		
					<span class="form-sub-label-container">
						<input id="index_inter_mess" name="index_inter_mess" value='<c:out value="${viewBean.index}"></c:out>' type="text" size="10">
						<label class="form-sub-label">Индекс</label>
					</span>
					<span class="form-sub-label-container">
						<input id="city_inter_mess" name="city_inter_mess" value='<c:out value="${viewBean.city}"></c:out>' type="text" size="15">
						<label class="form-sub-label">Город</label>
					</span>
					<span class="form-sub-label-container">
						<input id="street_inter_mess" name="street_inter_mess" value='<c:out value="${viewBean.street}"></c:out>' type="text" size="20">
						<label class="form-sub-label">Улица</label>
					</span>
					<span class="form-sub-label-container">
						<input id="home_inter_mess" name="home_inter_mess"value='<c:out value="${viewBean.home}"></c:out>' type="text" size="3">
						<label class="form-sub-label">Дом</label>
					</span>
					<span class="form-sub-label-container">
						<input id="housing_inter_mess" name="housing_inter_mess" value='<c:out value="${viewBean.housing}"></c:out>' type="text" size="3">
						<label class="form-sub-label">Корпус</label>
					</span>
					<span class="form-sub-label-container">
						<input id="apartment_inter_mess" name="apartment_inter_mess" value='<c:out value="${viewBean.apartment}"></c:out>' type="text" size="3">
						<label class="form-sub-label">Квартира</label>
					</span>
				</div>
				<!-- 
				<div class="form-input">
					<span class="form-sub-label-container"><label class="form-sub-label">Введите 0 в обязательное поле «Квартира», если ваш адрес заканчивается номером дома.</label></span>
				</div>
				 -->
			</li>
			
			</c:if>
			
			<li class="form-line form-line-column">
				<label class="form-label-left">Организация</label>
				<div class="form-input-wide">
					<span class="form-sub-label-container">
							<input id="organization_inter_mess" name="organization_inter_mess" value='<c:out value="${viewBean.organization}"></c:out>' type="text" size="92">
					</span>
		
				</div>
	        </li>
			
			<li class="form-line form-line-column">
				<label class="form-label-left">Контактная информация<span class="form-required">*</span></label>
				<div class="form-input-wide">
		
				<span class="form-sub-label-container">
					<input id="email_inter_mess" name="email_inter_mess" value='<c:out value="${viewBean.email}"></c:out>' type="text" size="30"
					class='<c:if test="${(viewBean.validEmail eq viewBean.idle) || (viewBean.validEmail eq viewBean.badEmail)}">form-validation-error</c:if>'>
					<label class="form-sub-label">Адрес электронной почты</label>
				</span>
				<span class="form-sub-label-container">
					<input id="number_phone_inter_mess" name="number_phone_inter_mess" value='<c:out value="${viewBean.numberPhone}"></c:out>' type="text" size="15">
					<label class="form-sub-label">Телефон</label>
				</span>
		                <!--validation-->
				<c:if test="${viewBean.validEmail eq viewBean.idle}">
					<div class="form-error-message">Необходимо ввести адрес электронной почты.</div>
				</c:if>
				<c:if test="${viewBean.validEmail eq viewBean.badEmail}">
					<div class="form-error-message">Адрес электронной почты введен некорректно.</div>
				</c:if>
		
				</div>
		        </li>
		
			<li class="form-line form-line-column">
				<label class="form-label-left">Текст обращения<span class="form-required">*</span></label>
				<div class="form-input-wide">
		
				<span class="form-sub-label-container">
					<textarea id="text_appeal_inter_mess" name="text_appeal_inter_mess" rows="15" cols="100"
					class='<c:if test="${viewBean.validTextAppeal eq viewBean.idle}">form-validation-error</c:if>'><c:out value="${viewBean.textAppeal}"></c:out></textarea>
				</span>
		                <!--validation-->
				<c:if test="${viewBean.validTextAppeal eq viewBean.idle}">
					<div class="form-error-message">Поле обязательно для заполнения.</div>
				</c:if>
		
				</div>
	        </li>
		
			<li class="form-line form-line-column">
				<label class="form-label-left">Прилагаемый файл</label>
				<div class="form-input-wide">
		
				<span class="form-sub-label-container">
					<input id="attach_inter_mess" name="attach_inter_mess" type="file"
					class='<c:if test="${(viewBean.validAttach eq viewBean.badSizeAttach) || (viewBean.validAttach eq viewBean.badFormatAttach)}">form-validation-error</c:if>'>
					<label class="form-sub-label">Файл <c:if test="${viewBean.editBean.formatAttach eq ''}">любого</c:if> формата <c:out value="${viewBean.editBean.formatAttach}"></c:out> размером не более <c:out value="${viewBean.editBean.maxSizeAttach}"></c:out> Мб <!--(<a href="http://admomsk.ru/web/guest/reception/questioning/manual">рекомендации</a>)--></label>
				</span>
		                <!--validation-->
				<c:if test="${viewBean.validAttach eq viewBean.badFormatAttach}">
					<div class="form-error-message">Недопустимый формат файла.</div>
				</c:if>
				<c:if test="${viewBean.validAttach eq viewBean.badSizeAttach}">
					<div class="form-error-message">Файл превышает допустимый размер.</div>
				</c:if>
		
				</div>
			</li>
			
			<c:if test="${not empty viewBean.editBean.urlVerify and not empty viewBean.editBean.siteKey and not empty viewBean.editBean.secretKey}">
				<li class="form-line form-line-column">
				
					<div class="form-input-wide">
						
						<span class="form-sub-label-container">
							<div class="g-recaptcha" data-sitekey="<c:out value="${viewBean.editBean.siteKey}"></c:out>" > </div>
						</span>
						
						<c:if test="${viewBean.validReCaptcha eq viewBean.idle}">
						  	<div class="form-error-message">Пожалуйста, подтвердите, что вы не робот.</div>
					</c:if>	
							
					</div>
					
				</li>
			</c:if>
			
			<li class="form-line form-line-column">
				<label class="form-sub-label">Заполнив форму и нажав кнопку &laquo;Подать обращение&raquo;, я даю свое согласие Администрации города Омска, находящейся по адресу: Омск, ул.&nbsp;Гагарина, 34, на передачу, обработку и хранение моих персональных данных, указанных выше, с использованием средств автоматизации с целью подготовки ответа по интересующему меня вопросу, изложенному выше, в соответствии с Федеральным законом от 27.07.2006 №&nbsp;152-ФЗ &laquo;О персональных данных&raquo;.</label>
			</li>
		
			<li class="form-line form-line-column">
				<div class="form-input-wide">
					<input id="btn_send_question_inter_mess" name="btn_send_question_inter_mess" type="submit" value="Подать обращение">
				</div>
			</li>
			
			
		</ul>
		
		</form>
		</div>
	
	</c:if>
	
	<c:if test="${not viewBean.viewForm}">
		<div id="kindergarden">
			
			<c:if test='<%=viewBean.getSentStatus().equals(viewBean.SENT_SED_SUCC)%>'>
				<div class="journal-content-article">
					<h2>Отправка завершена</h2>
					<p>Ваше обращение успешно отправлено. Благодарим вас за использование этого сервиса портала.</p>
					<p>В течение трех дней с момента подачи заявления на адрес электронной почты, указанный в заявлении, будет направлено уведомление. Оно подтвердит, что ваше обращение зарегистрировано и принято в работу.</p>
					<p>В случае неполучения такого уведомления в указанный срок необходимо позвонить в управление по работе с обращениями граждан Администрации города Омска по телефонам 20-08-02, 78-78-22.</p>
					<p>Вы можете вернуться к заполнению <a id="btn_back_notsucc_inter_mess" href="${viewUrl}">формы</a>.</p>
				</div>
			</c:if>
				
			<c:if test='<%=viewBean.getSentStatus().equals(viewBean.SENT_SED_FAIL)%>'>	
				<div class="journal-content-article">
					<c:choose>
						<c:when test="${viewBean.localizedMessage ne ''}">
							<h2>Ваше обращение не отправлено!</h2>
							<div class="form-error-message">Произошла ошибка: <c:out value="${viewBean.localizedMessage}"></c:out></div>
							<p>Попробуйте отправить обращение <a id="btn_back_notsucc_inter_mess" href="${viewUrl}">ещё раз</a>.</p>
						</c:when>
						<c:otherwise>
							<h2>Повторная отправка обращения не требуется</h2>
							<p>Вы повторно выполнили переход по ссылке из письма, направленного вам для завершения процедуры подачи обращения. Ничего страшного: обращение уже было подано ранее, не волнуйтесь.</p>
						</c:otherwise>
					</c:choose> 
				</div>
				<%--<portlet:renderURL var="viewUrl" portletMode="view"></portlet:renderURL>--%>
				<%--<a id="btn_back_notsucc_inter_mess" class="aqBtnVote" href="${viewUrl}">Назад</a>--%>
			</c:if>
		
			<c:if test='<%=viewBean.getSentStatus().equals(viewBean.SENT_USER_SUCC)%>'>
				<div class="journal-content-article">
					<h2>Отправка обращения: подтверждение адреса электронной почты</h2>
					<p>На указанный вами адрес электронной почты <strong>${viewBean.email}</strong> отправлено проверочное письмо со ссылкой.</p>
					<p>Пожалуйста, откройте присланное письмо и выполните переход по ссылке, чтобы подтвердить работоспособность вашего адреса электронной почты.</p>
					<p>Отправка вашего обращения будет завершена <strong>только после перехода по ссылке из письма</strong>.</p>
				</div>
			</c:if>
				
			<c:if test='<%=viewBean.getSentStatus().equals(viewBean.SENT_USER_FAIL)%>'>	
				<div class="journal-content-article">
					<h2>Ваше обращение не отправлено!</h2>
					<c:if test="${viewBean.localizedMessage ne ''}">
						<div class="form-error-message">Произошла ошибка: <c:out value="${viewBean.localizedMessage}"></c:out></div>
					</c:if>
					<p>Попробуйте отправить обращение <a id="btn_back_notsucc_inter_mess" href="${viewUrl}">ещё раз</a>.</p>
				</div>
			</c:if>
				
			<jsp:setProperty name="viewBean" property="viewForm" value="true" />
		</div>
	</c:if>
	
</c:if>